###To Start project:
    1. Clone this repo
    2. Run `yarn`
    3. Install typescript if it not install yet `npm i -g typescript` or 
       use yarn if you want
    4. Compile typescript files and watch for changes by running `tsc -w`
       @Note: for now there is be an error with types in react-redux, it will be fix later
    5. Run `react-native run-ios` to launch project in simulator